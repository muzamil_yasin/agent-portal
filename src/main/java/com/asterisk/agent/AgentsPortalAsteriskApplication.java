package com.asterisk.agent;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class AgentsPortalAsteriskApplication {

	public static void main(String[] args) {
		SpringApplication.run(AgentsPortalAsteriskApplication.class, args);
	}

}
